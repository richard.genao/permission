﻿using AutoMapper;
using PermisosProject.IRepository;
using PermisosProject.IServices;
using PermisosProject.Models;
using PermisosProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermisosProject.Services
{
    public class PermisosService : IPermisosService
    {
        private IRepositoryGeneric<Permiso> _repository;
        private IRepositoryGeneric<TipoPermiso> _tipoPermisorepository;
        private IMapper _mapper; 
        public PermisosService(IRepositoryGeneric<Permiso> repository, IRepositoryGeneric<TipoPermiso> tipoPermisorepository, IMapper mapper)
        {
            _repository = repository;
            _tipoPermisorepository = tipoPermisorepository;
            _mapper = mapper;
        }
        public async Task AddPermiso(PermisoDto permiso)
        {
            var result = _mapper.Map<PermisoDto, Permiso>(permiso);
            await _repository.Add(result);
        }
        public async Task Update(PermisoDto permiso)
        {
            var result = _mapper.Map<PermisoDto, Permiso>(permiso);
            await _repository.Update(result);
        }
        public  IEnumerable<PermisoDto> GetAllPermisos()
        {
            var result = _tipoPermisorepository.GetAll();
            return  _mapper.Map<IEnumerable<Permiso>, IEnumerable<PermisoDto>>(result);
        } 
        public void DeletePermiso(int Id)
        {
            var result = _repository.Find(x => x.Id == Id).Result.FirstOrDefault();
            _repository.Delete(result);
        }
        public IEnumerable<TipoPermiso> GetPermisos()
        {
            return  _tipoPermisorepository.GetAlltypes();
        }

        public  PermisoDto FindPermiso(int id)
        {
            var result = _repository.Find(x => x.Id == id).Result.FirstOrDefault();
            return _mapper.Map<Permiso, PermisoDto> (result);
        }
    }
}
