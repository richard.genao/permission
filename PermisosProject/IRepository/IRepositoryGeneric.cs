﻿using PermisosProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PermisosProject.IRepository
{
    public interface IRepositoryGeneric<T> where T : class
    {
        Task Add(T entity);
        Task Update(T entity);
        IEnumerable<Permiso> GetAll();
        IEnumerable<T> GetAlltypes();
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> include);
        void Delete(T Id);
    }
}
