﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PermisosProject.Models
{
    public class Permiso
    {
        [Key]
        public int Id { get; set; }
        public string NombreEmpleado { get; set; }
        public string ApellidosEmpleado { get; set; }
        [ForeignKey("TipoPermiso")]
        public int TipoPermisoId { get; set; }
        public DateTime FechaPermiso { get; set; }
        public  TipoPermiso TipoPermiso { get; set; }
    }
}
