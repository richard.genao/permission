﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermisosProject.Models
{
    public class PermisosContext:DbContext
    {
        public PermisosContext()
        {
        }

        public PermisosContext(DbContextOptions<PermisosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Permiso> Permiso { get; set; }
        public virtual DbSet<TipoPermiso> TipoPermiso { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer("Server=(localdb)\\local;Database=Prueba;Trusted_Connection=True;");
            }
        }
    }

