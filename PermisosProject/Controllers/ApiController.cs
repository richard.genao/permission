﻿using Microsoft.AspNetCore.Mvc;
using Prisiones.Domain.Core.ResponseResult;
using System;
using System.Linq;


namespace Prisiones.Services.Api.Controllers.Base
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ApiController : ControllerBase
    {
        protected new IActionResult Response(object entity = null,
            ResponseStatus.StatusCode code = ResponseStatus.StatusCode.Success,
            string message = null)
        {
            var message_return = (message != null) ? message
                : (code == ResponseStatus.StatusCode.SuccessCreate)
                        ? ResponseStatus.GetEnumDescription(ResponseStatus.StatusMessages.SuccessCreateMessage)
                        : ResponseStatus.GetEnumDescription(ResponseStatus.StatusMessages.SuccessUpdateMessage);


            if (code == ResponseStatus.StatusCode.SuccessCreate ||
                code == ResponseStatus.StatusCode.SuccessUpdate)
            {
                return Ok(new
                {
                    success = true,
                    code,
                    message = message_return
                });
            }

            if (entity != null && code == ResponseStatus.StatusCode.Success)
            {
                return Ok(new
                {
                    success = true,
                    code,
                    data = entity
                });
            }

            return BadRequest(new
            {
                success = false,
                code = ResponseStatus.StatusCode.NotFound,
                errors = ResponseStatus.GetEnumDescription(ResponseStatus.StatusMessages.NotFoundMessage)
            });
        }

        protected IActionResult Error(string message)
        {
            return BadRequest(new
            {
                success = false,
                code = ResponseStatus.StatusCode.ErrorMessage,
                errors = message
            });
        }

        protected IActionResult Error(Exception ex)
        {

            return BadRequest(new
            {
                success = false,
                code = ResponseStatus.StatusCode.ErrorRequest,
                message = ResponseStatus.GetEnumDescription(ResponseStatus.StatusMessages.ErrorRequest),
                errors = ex.Message
            });
        }

        protected IActionResult ErrorModelState(object obj)
        {
            return BadRequest(new
            {
                success = false,
                errors = ModelState.Values.SelectMany(v => v.Errors).Select(x => x.ErrorMessage),
                code = ResponseStatus.StatusCode.ErrorModelState
            });

        }
    }
}
