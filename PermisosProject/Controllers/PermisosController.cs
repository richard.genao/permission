﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PermisosProject.IServices;
using PermisosProject.Models;
using PermisosProject.ViewModels;
using Prisiones.Services.Api.Controllers.Base;

namespace PermisosProject.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PermisosController : ApiController
    {
        private IPermisosService _service;
        public PermisosController(IPermisosService service)
        {
            _service = service;
        }
        [HttpPost("create-permiso")]
        public async Task<IActionResult> AddPermiso(PermisoDto permiso)
        {
            try
            {
                await _service.AddPermiso(permiso);
                return Response();
            }
            catch (Exception e)
            {

                return Error(e);
            }
        }
        [HttpGet("get-all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var result =  _service.GetAllPermisos();
                return Response(result);
            }
            catch (Exception e)
            {

                return Error(e);
            }
        }
        [HttpPut("update-permiso")]
        public async Task<IActionResult> UpdatePermiso(PermisoDto permiso)
        {
            try
            {
                await _service.Update(permiso);
                return Response();
            }
            catch (Exception e)
            {

                return Error(e);
            }
        }
        [HttpDelete("delete-permiso/{Id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            try
            {
                 _service.DeletePermiso(Id);
                return Response();
            }
            catch (Exception e)
            {

                return Error(e);
            }
        }
        [HttpGet("get-types")]
        public async Task<IActionResult> GetTypes()
        {
            try
            {
                var result =  _service.GetPermisos();
                return Response(result);
            }
            catch (Exception e)
            {

                return Error(e);
            }
            

        }
        [HttpGet("find-permiso/{id}")]
        public IActionResult FindPermiso(int id)
        {
            try
            {
                var result =  _service.FindPermiso(id);
                return Response(result);
            }
            catch (Exception e)
            {

                return Error(e);
            }
        }
    }
}