﻿
using AutoMapper;
using PermisosProject.Models;
using PermisosProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermisosProject.Mapper
{
    public class MapHelper : Profile
    {
        public MapHelper()
        {
            CreateMap<PermisoDto, Permiso>()
                .ForMember(x => x.NombreEmpleado, opt => opt.MapFrom(s => s.Name))
                .ForMember(x => x.ApellidosEmpleado, opt => opt.MapFrom(s => s.LastName))
                .ForMember(x => x.FechaPermiso, opt => opt.MapFrom(s => s.Date))
                .ForMember(x => x.TipoPermisoId, opt => opt.MapFrom(s => s.TypePermissionId));


            CreateMap<Permiso, PermisoDto>()
                .ForMember(x => x.Name, opt => opt.MapFrom(s => s.NombreEmpleado))
                .ForMember(x => x.LastName, opt => opt.MapFrom(s => s.ApellidosEmpleado))
                .ForMember(x => x.Date, opt => opt.MapFrom(s => s.FechaPermiso))
                .ForMember(x => x.TypePermissionId, opt => opt.MapFrom(s => s.TipoPermisoId))
                .ForMember(x => x.TypePermission, opt => opt.MapFrom(s => s.TipoPermiso.Descripcion));


        }
    }
}
