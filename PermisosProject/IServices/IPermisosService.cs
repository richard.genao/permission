﻿using PermisosProject.Models;
using PermisosProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermisosProject.IServices
{
    public interface IPermisosService
    {
        Task AddPermiso(PermisoDto permiso);

        Task Update(PermisoDto permiso);

        IEnumerable<PermisoDto> GetAllPermisos();

        void DeletePermiso(int Id);
        IEnumerable<TipoPermiso> GetPermisos();
        PermisoDto FindPermiso(int id);

    }
}
