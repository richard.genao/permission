﻿using Microsoft.EntityFrameworkCore;
using PermisosProject.IRepository;
using PermisosProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PermisosProject.Repository
{
    public class RepositoryGeneric<T> : IRepositoryGeneric<T> where T : class
    {
        private PermisosContext _context;
        private DbSet<T> _dbSet;
        public RepositoryGeneric(PermisosContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }
        public  IEnumerable<T> GetAlltypes()
        {
            return  _dbSet.ToList();           
        }
        public IEnumerable<Permiso> GetAll()
        {
            return _context.Permiso.Include(x => x.TipoPermiso).ToList();
        }
        public async Task Add(T entity)
        {
            await _context.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public void Delete(T Id)
        {
            _context.Remove(Id);
            _context.SaveChanges();
        }

        public async Task<IEnumerable<T>> Find(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }

        public async Task Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> include)
        {
            return await _dbSet.Include(include).Where(predicate).ToListAsync();
        }

    }
}
