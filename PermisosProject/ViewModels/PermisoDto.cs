﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermisosProject.ViewModels
{
    public class PermisoDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int TypePermissionId { get; set; }
        public string TypePermission { get; set; }
        public string Date { get; set; }
             
    }
}
