﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PermisosProject.IRepository;
using PermisosProject.IServices;
using PermisosProject.Repository;
using PermisosProject.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PermisosProject.Dependency
{
    public class DependencyInjector
    {
        public static void AddDbContext<T>(IServiceCollection services, string connectionString) where T : DbContext
        {
            services.AddDbContext<T>(options => options.UseSqlServer(connectionString));
        }

        public static void RegisterServices(IServiceCollection services)
        {
            //Data
            services.AddTransient(typeof(IRepositoryGeneric<>), typeof(RepositoryGeneric<>));

            //Services
            services.AddScoped<IPermisosService, PermisosService>();

        }
    }
}
