import Vue from 'vue';
import Router from 'vue-router';
import Add from './Add.vue'
import Edit from './Edit.vue';
import List from './List.vue';

Vue.use(Router)
export default new Router({
    mode:'history',
//    base: process.env.BASE_URL,
    routes:[
    {
        path:"/",
        name:"add",
        component: Add,

    },
    {
        path:"/edit/:id",
        name:"edit",
        component: Edit,

    },
    {
        path:"/list",
        name:"list",
        component: List,

    },
    

]

})